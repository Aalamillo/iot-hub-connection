**Actividad 2 - Frameworks centralizados y distribuidos**

Aquí están los archivos para realizar la actividad 2

* register-device.js: Registra un nuevo dispositivo en el IoT Hub
* simulated-device.js: Envía datos al IoT Hub
* read-iot-messages.js: Recibe datos del IoT Hub
* package.json: Archivo de dependencias de node.js
* .gitignore: Archivo exclusivo para GIT. Ignora los ficheros y carpetas que no se deben subir

---

## Instrucciones

Para que funcione el ejemplo lo primero que se debe hacer es sutituir los azure tokens por los correspondientes.

Después ejecutar el siguiente comando:

npm install